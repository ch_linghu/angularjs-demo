var MyAppModule = angular.module("myApp", ['ngRoute'])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider
            .when('/list', {templateUrl: 'templates/list.html',
                controller: 'ListController'})
            .when('/detail/:id', {templateUrl: 'templates/detail.html',
                controller: 'DetailController'})
            .otherwise({redirectTo: '/list'});
    }]);
