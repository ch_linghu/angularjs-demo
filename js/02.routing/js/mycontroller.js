MyAppModule
    .controller("ListController", ['$scope', function($scope){
        $scope.details = new Array();
        for(var i = 0; i < 10; i++){
            $scope.details.push({"index":i, "tags": Math.floor(Math.random() * 100)});
        }
    }])
    .controller("DetailController", ['$scope', '$routeParams', function($scope, $routeParams){
        $scope.id = $routeParams['id'];
        $scope.tags = $routeParams['tags'];
    }]);
