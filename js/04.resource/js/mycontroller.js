var myController = angular.module('MyController', []);

myController
    .controller("ListController", ['$scope', 'Object', function($scope, Object){
        $scope.objects = Object.query();
    }])
    .controller("DetailController", ['$scope', '$routeParams', 'Object', function($scope, $routeParams, Object){
        $scope.id = $routeParams['id'];
        $scope.obj = Object.get({id:$scope.id});
    }]);
