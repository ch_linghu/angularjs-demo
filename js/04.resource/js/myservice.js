var myService = angular.module('MyService', ['ngResource']);

myService.factory('Object', ['$resource', function($resource){
        return $resource('/api/object/:id', {});
    }]);
