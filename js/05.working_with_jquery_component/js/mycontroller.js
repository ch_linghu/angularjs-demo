var myController = angular.module('MyController', []);

myController
    .controller("PlotController", ['$scope', function($scope){
        $scope.a = 1;
        $scope.b = 2;
        $scope.c = 3;
        $scope.d = 4;
        $scope.chart_data = [{
            data: [[0,$scope.a],[1,$scope.b],[2,$scope.c],[3,$scope.d]]
        }];

        function update(idx, value){
            var new_chart_data = [{
                data:$scope.chart_data[0].data
            }];
            console.log("new chart data:", new_chart_data);
            for(i = 0; i < new_chart_data[0].data.length; i++){
                if (new_chart_data[0].data[i][0] == idx){
                    new_chart_data[0].data[i][1] = value;
                    break;
                }
            }
            $scope.chart_data = new_chart_data;
        }

        $scope.$watch('a', function(v){
            update(0, v);
        });
        $scope.$watch('b', function(v){
            update(1, v);
        });
        $scope.$watch('c', function(v){
            update(2, v);
        });
        $scope.$watch('d', function(v){
            update(3, v);
        });
    }]);
