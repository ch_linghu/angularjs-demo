var myDirective = angular.module('MyDirective', []);

myDirective
    .directive('myFlot', function(){
        return {
            restrict: 'E',
            link: function (scope, elem, attrs){
                var chart = null,
                    opts = {};

                scope.$watch(attrs.ngModel, function(v){
                    console.log("ngModel changed to ", v);
                    if (!chart){
                        chart = $.plot(elem, v, opts);
                        elem.show();
                    }else{
                        chart.setData(v);
                        chart.setupGrid();
                        chart.draw();
                    }
                });
            }
        };
    });
