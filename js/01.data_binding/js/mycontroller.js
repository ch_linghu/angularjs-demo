angular.module('myApp', [])
    .controller("MyController", function($scope){
        $scope.a = 1;
        $scope.b = 2;

        $scope.arr = new Array();
        $scope.arr.push("item_1");
        $scope.arr.push("item_2");

        $scope.set_a_b = function (new_a, new_b){
            $scope.a = new_a;
            $scope.b = new_b;
        }

        $scope.add_to_arr = function () {
            $scope.arr.push("item_" + ($scope.arr.length+1));
        }

        $scope.delete_from_arr = function (index) {
            $scope.arr.splice(index, 1);
        }
    });
