app_1
    .controller("ListController", ['$scope', function($scope){
        $scope.details = new Array();
        for(var i = 0; i < 10; i++){
            $scope.details.push(i);
        }
    }])
    .controller("DetailController", ['$scope', '$routeParams', function($scope, $routeParams){
        $scope.id = $routeParams['id'];
        $scope.tags = $routeParams['tags'];
    }]);
