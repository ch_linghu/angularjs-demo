var app_1 = angular.module("app-1", ['ngRoute'])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider
            .when('/list', {templateUrl: 'app_1/templates/list.html',
                controller: 'ListController'})
            .when('/detail/:id', {templateUrl: 'app_1/templates/detail.html',
                controller: 'DetailController'})
            .otherwise({redirectTo: '/list'});
    }]);
