MyAppModule
    .controller("ListController", ['$scope', '$http', function($scope, $http){
        $http.get("/api/object").success(function(data, status, headers, config) {
            $scope.objects = data;
        })
    }])
    .controller("DetailController", ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http){
        $scope.id = $routeParams['id'];
        $http.get("/api/object/"+$scope.id).success(function(data, status, headers, config){
            $scope.obj = data
        })
    }]);
