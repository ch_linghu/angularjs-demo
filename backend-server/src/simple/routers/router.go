package routers

import (
	"simple/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.RESTRouter("/object", &controllers.ObjectController{})
}
