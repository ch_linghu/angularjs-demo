package models

import (
	"errors"
	"strconv"
	"time"
)

var (
	Objects []*Object
)

type Object struct {
	ObjectId   string
	Score      int64
	PlayerName string
}

func init() {
	Objects = make([]*Object, 0)
	Objects = append(Objects, &Object{"hjkhsbnmn123", 100, "astaxie"})
	Objects = append(Objects, &Object{"mjjkxsxsaa23", 101, "someone"})
}

func AddOne(object Object) (ObjectId string) {
	object.ObjectId = "astaxie" + strconv.FormatInt(time.Now().UnixNano(), 10)
	Objects = append(Objects, &object)
	return object.ObjectId
}

func GetOne(ObjectId string) (object *Object, err error) {
    for _, obj := range Objects {
        if obj.ObjectId == ObjectId {
            return obj, nil
        }
    }
	return nil, errors.New("ObjectId Not Exist")
}

func GetAll() []*Object {
	return Objects
}

func Update(ObjectId string, Score int64) (err error) {
    for index, obj := range Objects {
        if obj.ObjectId == ObjectId {
            Objects[index].Score = Score
            return nil
        }
    }
	return errors.New("ObjectId Not Exist")
}

func Delete(ObjectId string) {
    for index, obj := range Objects {
        if obj.ObjectId == ObjectId {
            Objects = append(Objects[:index], Objects[index+1:]...)
            return
        }
    }
}
