# README #

这是《AngularJS简介》配套的演示代码

这个demo分成前台和后台两个部分

后台的代码在 backend-server 目录中。它是基于 golang (http://golang.org) 和 beego (http://beego.me) 完成的。
其中beego的代码已经包含其中，只需要go就可以编译运行。

前台的代码在 js 目录中。分成6个部分。

1. 演示数据绑定
1. 演示路由功能
1. 演示与后台API的交互
1. 演示resource（与后台交互方式的改进）
1. 演示directive（与第三方库的集成）
1. 演示多app

其中3和4需要后台服务支持，因此需要事先启动后台服务。其它的演示无需后台支持。

为了方便，所有演示都自带了对 goteway (https://bitbucket.org/ch_linghu/goteway) 的支持。
只要下载 goteway 并编译，将编译出的可执行程序放入搜索目录，然后 cd 到特定的演示目录下(如 01.data_binding)，执行 goteway ，就可以启动一个http服务。然后在
浏览器中输入 http://127.0.0.1:8000 即可看到对应演示目录的演示结果